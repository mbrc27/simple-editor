## Simple editor

The App is based on CRA, to run in dev just type in command:

```
 yarn run dev
```

### NOTE

Since this is a recruitment task i decided to expermint with hooks API for store management. I've also decided for a TypeScript here, because even in simple projects it brings some benefits.

Save/Load uses localStorage and it stores the store. Item positions aren't stored within so that's why it's going to the default position.

About typings within state actions, this is a very very simplified version, normally we would want some better action type union, but in this case to keep it simple i've used explicit type projection within reducers.

About the directory structure. In this case it's plain old components, containers etc. structure. Because it's small and simple project. But normally you would want to keep it in more modular way (e.g. redux ducks), but also to keep it simple, without too many boilerplate i've decided to use this structure.
