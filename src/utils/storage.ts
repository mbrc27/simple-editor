const EDITOR_KEY = "EDITOR_KEY";

export const saveState = <T extends Object = {}>(state: T, logger = alert) => {
  try {
    localStorage.setItem(EDITOR_KEY, JSON.stringify(state));
    logger("Successfully saved progress!");
  } catch (error) {
    console.error(error);
    logger("Could not save progress");
  }
};

export const loadState = (logger = alert) => {
  try {
    const state = localStorage.getItem(EDITOR_KEY);

    return state ? JSON.parse(state) : {};
  } catch (error) {
    console.error(error);
    logger("Could not restore progress");
  }
};
