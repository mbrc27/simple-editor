import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;
  height: 100%;
  padding: 0 25px;
`;

export const Title = styled.h2`
  align-self: center;
`;
