import React from "react";

import {
  backgroundState as background,
  backgroundActions,
  backgroundReducer
} from "state/backgroud";
import { logoState as logo, logoActions, logoReducer } from "state/logo";
import { textState as text, textActions, textReducer } from "state/text";
import { State } from "state/types";

enum coreActions {
  RESTORE = "RESTORE"
}

const initialState: State = {
  background,
  logo,
  text
};

export type Action = {
  type: backgroundActions | logoActions | textActions | coreActions;
  payload?: string | string[] | State;
};

function reducer(state: State, action: Action) {
  if (action.type === coreActions.RESTORE) {
    return action.payload as State;
  }

  return {
    background: backgroundReducer(state.background, action),
    logo: logoReducer(state.logo, action),
    text: textReducer(state.text, action)
  };
}

export function useStore() {
  const [state, dispatch] = React.useReducer(reducer, initialState);

  return [
    state,
    dispatch,
    (payload: State) => dispatch({ type: coreActions.RESTORE, payload })
  ] as const;
}
