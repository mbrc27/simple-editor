import React from "react";

import { fetchBackgrounds } from "api/background";
import { Action } from "state";
import { EditorContext } from "state/provider";

export enum backgroundActions {
  ADD = "background/ADD",
  SET = "background/SET",
  DELETE = "background/DELETE"
}

export interface BackgroundState {
  available: string[];
  selected: string;
  default: string;
}

export const backgroundState: BackgroundState = {
  available: [],
  selected: "empty_background.png",
  default: "empty_background.png"
};

export function backgroundReducer(state: BackgroundState, action: Action) {
  switch (action.type) {
    case backgroundActions.ADD:
      return {
        ...state,
        available: action.payload as string[]
      };
    case backgroundActions.SET:
      return {
        ...state,
        selected: action.payload as string
      };
    case backgroundActions.DELETE:
      return {
        ...state,
        selected: state.default
      };
    default:
      return state;
  }
}

export function useBackground() {
  const {
    state: { background },
    dispatch
  } = React.useContext(EditorContext);

  const addBackground = React.useCallback(
    (payload: any) => dispatch({ type: backgroundActions.ADD, payload }),
    [dispatch]
  );

  const setBackground = React.useCallback(
    (payload: string) => dispatch({ type: backgroundActions.SET, payload }),
    [dispatch]
  );
  const deleteBackground = React.useCallback(
    () => dispatch({ type: backgroundActions.DELETE }),
    [dispatch]
  );

  React.useEffect(() => {
    const fetchData = async () => {
      const data = await fetchBackgrounds();

      addBackground(data);
    };

    fetchData();
  }, [addBackground]);

  return { background, addBackground, setBackground, deleteBackground };
}
