import React from "react";
import { Action } from "state";
import { EditorContext } from "state/provider";

export enum logoActions {
  ADD = "logo/ADD",
  DELETE = "logo/DELETE"
}

export interface LogoState {
  available: string[];
  selected: string[];
}

export const logoState: LogoState = {
  available: [
    "logos/logo_one.png",
    "logos/logo_two.png",
    "logos/logo_three.png"
  ],
  selected: []
};

export function logoReducer(state: LogoState, action: Action) {
  switch (action.type) {
    case logoActions.ADD:
      return {
        ...state,
        selected: [...state.selected, action.payload as string]
      };
    case logoActions.DELETE:
      return {
        ...state,
        selected: state.selected.filter(
          logo => logo !== (action.payload as string)
        )
      };
    default:
      return state;
  }
}

export function useLogo() {
  const {
    state: { logo },
    dispatch
  } = React.useContext(EditorContext);

  const addLogo = React.useCallback(
    (payload: string) => dispatch({ type: logoActions.ADD, payload }),
    [dispatch]
  );

  const deleteLogo = React.useCallback(
    (payload: string) => dispatch({ type: logoActions.DELETE, payload }),
    [dispatch]
  );

  return { logo, addLogo, deleteLogo };
}
