import React from "react";
import { Action } from "state";
import { EditorContext } from "./provider";

export enum textActions {
  ADD = "text/ADD",
  DELETE = "text/DELETE",
  CHANGE_FONT = "text/CHANGE_FONT"
}

export interface TextState {
  text: string;
  fontFamily: string;
  fontsAvaiable: string[];
}

export const textState: TextState = {
  text: "",
  fontFamily: "Arial",
  fontsAvaiable: ["Arial", "Times New Roman", "Open Sans"]
};

export function textReducer(state: TextState, action: Action) {
  switch (action.type) {
    case textActions.ADD:
      return { ...state, text: action.payload! as string };
    case textActions.DELETE:
      return { ...state, text: "" };
    case textActions.CHANGE_FONT:
      return { ...state, fontFamily: action.payload! as string };
    default:
      return state;
  }
}

export function useText() {
  const {
    state: { text },
    dispatch
  } = React.useContext(EditorContext);

  const addText = React.useCallback(
    (payload: string) => dispatch({ type: textActions.ADD, payload }),
    [dispatch]
  );

  const deleteText = React.useCallback(
    () => dispatch({ type: textActions.DELETE }),
    [dispatch]
  );

  const changeFont = React.useCallback(
    (payload: string) => dispatch({ type: textActions.CHANGE_FONT, payload }),
    [dispatch]
  );

  return { text, addText, deleteText, changeFont };
}
