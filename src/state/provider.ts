import React from "react";
import { Action } from "state";
import { State } from "state/types";

export const EditorContext = React.createContext<{
  dispatch: React.Dispatch<Action>;
  state: State;
}>({
  dispatch: () => {},
  state: {} as any
});
