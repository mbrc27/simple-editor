import { BackgroundState } from "./backgroud";
import { TextState } from "./text";
import { LogoState } from "./logo";

export interface State {
  background: BackgroundState;
  text: TextState;
  logo: LogoState;
}
