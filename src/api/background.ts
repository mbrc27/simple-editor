export const fetchBackgrounds = async () => {
  try {
    const response = await fetch(
      // NOTE make some random pics request
      `https://picsum.photos/v2/list?limit=4&page=${Math.floor(
        Math.random() * 100
      ) + 1}`
    );

    // NOTE simplified API response type :)
    const data: { download_url: string }[] = await response.json();

    return data.map(i => i.download_url);
  } catch (error) {
    console.error(error);

    return [];
  }
};
