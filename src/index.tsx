import React from "react";
import ReactDOM from "react-dom";

import { App } from "app/containers";
import { GlobalStyle } from "styles/Global";

ReactDOM.render(
  <>
    <GlobalStyle />
    <App />
  </>,
  document.getElementById("root")
);
