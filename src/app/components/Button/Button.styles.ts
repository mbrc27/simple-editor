import styled from "styled-components";

export const Button = styled.button`
  padding: 10px;
  margin: 10px;
  background: none;
  border: none;
  color: black;
  box-shadow: 0 0 5px 1px #888888;
  font-size: 14px;
  font-weight: 600;
  cursor: pointer;
  transition: all 200ms ease-out;

  :hover {
    box-shadow: 0 0 5px 1px #23adff;
  }
`;
