import React, { ButtonHTMLAttributes } from "react";

import * as S from "./Button.styles";

export const Button: React.FC<ButtonHTMLAttributes<unknown>> = ({
  children,
  ...props
}) => <S.Button {...props}>{children}</S.Button>;
