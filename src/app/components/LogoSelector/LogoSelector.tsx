import React from "react";
import { generate } from "shortid";

import * as S from "./LogoSelector.styles";

interface Props {
  logos: string[];
}

export const LogoSelector = React.memo<Props>(({ logos }) => {
  const onDragStart = React.useCallback(
    (evt: React.DragEvent<HTMLImageElement>) => {
      const uri = evt.currentTarget.getAttribute("src");
      evt.dataTransfer.setData("uri", uri!);
    },
    []
  );
  return (
    <S.LogoWrapper>
      <S.LogoTitle>Add logo</S.LogoTitle>
      <S.LogosWrapper>
        {logos.map(logoUri => (
          <S.LogoImage
            key={generate()}
            alt="logo image"
            src={logoUri}
            onDragStart={onDragStart}
          />
        ))}
      </S.LogosWrapper>
    </S.LogoWrapper>
  );
});
