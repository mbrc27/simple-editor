import styled from "styled-components";
import { Title } from "styles";

export const LogoWrapper = styled.div`
  display: flex;
  flex-flow: column nowrap;
  height: 30%;
`;

export const LogoTitle = styled(Title)``;

export const LogosWrapper = styled.div`
  display: flex;
  justify-content: space-around;
`;

export const LogoImage = styled.img.attrs({ width: 100, height: 100 })`
  margin: 10px;
  border: solid 1px lightgray;
  cursor: pointer;
`;
