import React from "react";
import { Group, Text } from "react-konva";
import { ImageConfig } from "konva/types/shapes/Image";
import useImage from "use-image";

interface Props {
  uri?: string;
  onDelete?: (uri: string) => void;
  component: React.ComponentType<any>;
}

export const Drawing: React.FC<Props & Partial<ImageConfig>> = ({
  uri,
  onDelete,
  draggable,
  component: Component,
  ...props
}) => {
  const [image] = useImage(uri, "Anonymous");
  const [showDelete, switchDelete] = React.useState(false);

  const onDeleteClick = React.useCallback(
    () => onDelete && onDelete(uri || ""),
    [onDelete, uri]
  );
  const onDrawingClick = React.useCallback(() => switchDelete(!showDelete), [showDelete]);

  return (
    <Group draggable={draggable}>
      <Component image={image} {...props} onClick={onDrawingClick} />
      {!!onDelete && showDelete && (
        <Text
          onClick={onDeleteClick}
          height={25}
          width={25}
          text={"X"}
          fill="red"
          fontSize={25}
        />
      )}
    </Group>
  );
};
