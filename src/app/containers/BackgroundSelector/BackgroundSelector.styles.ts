import styled from "styled-components";
import { Wrapper, Title } from "styles";

export const BackgroundWrapper = styled(Wrapper)``;

export const BackGroundTitle = styled(Title)``;

export const BackgroundList = styled.div`
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
`;

export const BackgroundImage = styled.img.attrs({ width: 100, height: 100 })`
  margin: 10px;
  border: solid 1px lightgray;
  cursor: pointer;
`;
