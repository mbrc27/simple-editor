import React from "react";
import { generate } from "shortid";

import { Button } from "app/components";
import { useBackground } from "state/backgroud";
import * as S from "./BackgroundSelector.styles";

export const BackgroundSelector = React.memo(() => {
  const {
    background: { available },
    setBackground,
    deleteBackground
  } = useBackground();

  return (
    <S.BackgroundWrapper>
      <S.BackGroundTitle>Select background</S.BackGroundTitle>
      <S.BackgroundList>
        {available.map(backgroundUri => (
          <S.BackgroundImage
            key={generate()}
            alt="background image"
            src={backgroundUri}
            onClick={() => setBackground(backgroundUri)}
          />
        ))}
      </S.BackgroundList>
      <Button onClick={deleteBackground}>Delete background</Button>
    </S.BackgroundWrapper>
  );
});
