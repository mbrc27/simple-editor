import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 auto;
  width: 1200px;
  height: 100vh;
`;

export const Section = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  border: solid 1px grey;
`;

export const Main = styled.main`
  width: 500px;
  height: 600px;
`;

export const Side = styled.aside`
  display: flex;
  flex-flow: column nowrap;
  width: 450px;
  height: 600px;
  padding: 0 10px;
`;
