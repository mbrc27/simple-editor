import React from "react";

import { Button, LogoSelector } from "app/components";
import { BackgroundSelector, Editor, TextEditor } from "app/containers";
import { useStore } from "state";
import { EditorContext } from "state/provider";
import { saveState, loadState } from "utils/storage";

import * as S from "./App.styles";

export const App: React.FC = () => {
  const [state, dispatch, restore] = useStore();

  const onSaveClick = React.useCallback(() => saveState(state), [state]);
  const onLoadClick = React.useCallback(() => {
    const state = loadState();
    restore(state);
  }, [restore]);

  return (
    <EditorContext.Provider value={{ dispatch, state }}>
      <S.Wrapper>
        <S.Section>
          <S.Side>
            <BackgroundSelector />
          </S.Side>
          <S.Main>
            <Editor />
          </S.Main>
          <S.Side>
            <LogoSelector logos={state.logo.available} />
            <TextEditor />
            <Button onClick={onSaveClick}>Save</Button>
            <Button onClick={onLoadClick}>Load</Button>
          </S.Side>
        </S.Section>
      </S.Wrapper>
    </EditorContext.Provider>
  );
};
