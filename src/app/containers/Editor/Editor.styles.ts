import styled from "styled-components";
import { Wrapper, Title } from "styles";

export const EditorWrapper = styled(Wrapper)``;

export const EditorTitle = styled(Title)``;

export const StageWrapper = styled.div`
  border: solid 1px #888888;
  border-style: dotted;
`;
