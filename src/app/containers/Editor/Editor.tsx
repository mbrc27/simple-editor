import React from "react";
import { Stage, Layer, Image as KonvaImage, Text } from "react-konva";

import { Button, Drawing } from "app/components";
import * as S from "./Editor.styles";
import { useLogo } from "state/logo";
import { useText } from "state/text";
import { useBackground } from "state/backgroud";

export const Editor = React.memo(() => {
  const { logo, addLogo, deleteLogo } = useLogo();
  const { text, deleteText } = useText();
  const { background } = useBackground();
  const canvas = React.useRef<HTMLCanvasElement>(null);

  const onDrop = React.useCallback(
    (evt: React.DragEvent<HTMLDivElement>) => {
      const uri = evt.dataTransfer.getData("uri");
      addLogo(uri);
    },
    [addLogo]
  );

  const onDownloadClick = React.useCallback(() => {
    if (canvas.current) {
      const link = document.createElement("a");
      document.body.appendChild(link);
      link.download = "filename.png";
      link.href = canvas.current.toDataURL();
      link.click();
      document.body.removeChild(link);
    }
  }, []);

  return (
    <S.EditorWrapper>
      <S.EditorTitle>Simple Editor</S.EditorTitle>
      <S.StageWrapper onDrop={onDrop} onDragOver={e => e.preventDefault()}>
        <Stage width={400} height={400} ref={canvas as any}>
          <Layer>
            <Drawing
              component={KonvaImage}
              uri={background.selected}
              width={400}
              height={400}
            />
            {logo.selected.map(logo => (
              <Drawing
                component={KonvaImage}
                onDelete={deleteLogo}
                key={logo}
                uri={logo}
                width={100}
                height={100}
                draggable
              />
            ))}
            {!!text.text && (
              <Drawing
                component={Text}
                onDelete={deleteText}
                key={text.text}
                fontSize={50}
                fill={"black"}
                {...text}
                draggable
              />
            )}
          </Layer>
        </Stage>
      </S.StageWrapper>
      <Button onClick={onDownloadClick}>Download as image</Button>
    </S.EditorWrapper>
  );
});
