import styled from "styled-components";
import { Title } from "styles";

export const TextWrapper = styled.div`
  display: flex;
  flex-flow: column nowrap;
  height: 50%;
`;

export const TextTitle = styled(Title)``;

export const FontChooser = styled.div`
  display: flex;
  flex-flow: column nowrap;
`;
