import React from "react";

import * as S from "./TextEditor.styles";
import { Button } from "app/components";
import { useText } from "state/text";

export const TextEditor = React.memo(() => {
  const {
    text: { fontFamily, fontsAvaiable },
    addText,
    changeFont
  } = useText();
  const [text, setText] = React.useState("");
  const onTextChange = React.useCallback(
    (evt: React.ChangeEvent<HTMLInputElement>) =>
      setText(evt.currentTarget.value),
    []
  );
  const onAddClick = React.useCallback(() => addText(text), [addText, text]);
  const onFontChange = React.useCallback(
    (evt: React.ChangeEvent<HTMLInputElement>) =>
      changeFont(evt.currentTarget.value),
    [changeFont]
  );

  return (
    <S.TextWrapper>
      <S.TextTitle>Add text</S.TextTitle>
      <input type="text" value={text} onChange={onTextChange} />
      <S.FontChooser>
        {fontsAvaiable.map(font => (
          <label key={font}>
            <input
              type="radio"
              name="font"
              value={font}
              checked={font === fontFamily}
              onChange={onFontChange}
            />
            {font}
          </label>
        ))}
      </S.FontChooser>
      <Button onClick={onAddClick}>Add text</Button>
    </S.TextWrapper>
  );
});
